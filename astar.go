package main

import "math"

/*Search is an A-star search implementation*/
func Search(graph *[]*Node, start, goal *Node) ([]*Node, bool) {
	if graph == nil || start == nil || goal == nil {
		// log.Println("Search failed. Cannot be empty or null.")
		// log.Printf("Received: graph: %+v start: %+v goal: %+v\n", graph, start, goal)
		return nil, false
	}

	var openSet, closedSet, foundPath []*Node
	found := false

	// This set contains all the nodes that have been discovered but not evaluated yet
	// Start with the start node
	openSet = append(openSet, start)

	// map that contain how the node was achieved and where it came from
	cameFrom := make(map[*Node]*Node)

	// map that contain the actual cost and total cost
	gCost := make(map[*Node]float64)
	fCost := make(map[*Node]float64)

	// The start node has 0 cost from the start node itself
	gCost[start] = 0.0

	// The total cost from the start to end goal is the same with the heuristic cost
	fCost[start] = calculateHeuristicCost(start, goal)

	for len(openSet) > 0 {
		openSetIndex, currentNode := lowestFCost(&openSet, fCost)
		if currentNode == goal {
			foundPath = reconstructPath(cameFrom, currentNode)
			found = true
			break
		}
		closedSet = append(closedSet, currentNode)
		openSet = removeNode(openSet, openSetIndex)

		for _, neighbor := range currentNode.InternalNeighbors {
			if isNodeInSet(closedSet, neighbor) {
				continue
			}

			tentativeGCost := gCost[currentNode] + calculateCost(currentNode, neighbor)

			if isNodeInSet(openSet, neighbor) == false {
				openSet = append(openSet, neighbor) // New node to evaluate
			} else if tentativeGCost >= gCost[neighbor] {
				continue // the lowest actual cost is already evaluated
			}

			//Best path so far
			cameFrom[neighbor] = currentNode
			gCost[neighbor] = tentativeGCost
			fCost[neighbor] = tentativeGCost + calculateHeuristicCost(neighbor, goal)
		}
	}

	return foundPath, found
}

func calculateHeuristicCost(start, goal *Node) float64 {
	//Use distance formula to calculate the heuristic distance
	return calculateCost(start, goal)
}

func calculateCost(start, goal *Node) float64 {
	xsqr := float64((start.Coordinate.X - goal.Coordinate.X) * (start.Coordinate.X - goal.Coordinate.X))
	ysqr := float64((start.Coordinate.Y - goal.Coordinate.Y) * (start.Coordinate.Y - goal.Coordinate.Y))
	zsqr := float64((start.Coordinate.Z - goal.Coordinate.Z) * (start.Coordinate.Z - goal.Coordinate.Z))

	return math.Sqrt(xsqr + ysqr + zsqr)
}

func lowestFCost(set *[]*Node, fCost map[*Node]float64) (int, *Node) {
	var currentNode *Node
	tmpCost := 10000.0
	index := -1
	for i, n := range *set {
		if fCost[n] < tmpCost {
			currentNode = n
			index = i
			tmpCost = fCost[n]
		}
	}

	return index, currentNode
}

func removeNode(set []*Node, index int) []*Node {
	return append(set[:index], set[index+1:]...)
}

func isNodeInSet(set []*Node, target *Node) bool {
	exists := false
	for _, n := range set {
		if n == target {
			exists = true
			break
		}
	}
	return exists
}

func reconstructPath(cameFrom map[*Node]*Node, current *Node) []*Node {
	var path []*Node
	tmp := current
	path = append(path, tmp)

	for cameFrom[tmp] != nil {
		tmp = cameFrom[tmp]
		path = append(path, tmp)
	}

	return path
}
