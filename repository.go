package main

import (
	"context"

	pb "gitlab.com/lazybasterds/alpaca/map-service/proto/map"
)

//Repository is an interface for getting information from the database.
type Repository interface {
	GetPath(ctx context.Context, start, goal *pb.Node) ([]*pb.Node, error)
}

//MapRepository concrete implementation of the Repository interface.
type MapRepository struct {
}

// GetPath finds the path in the building map
func (repo *MapRepository) GetPath(ctx context.Context, start, goal *pb.Node) ([]*pb.Node, error) {

	startNode := getNodeByID(&buildingMap, start.Id)
	goalNode := getNodeByID(&buildingMap, goal.Id)

	path, _ := Search(&buildingMap, startNode, goalNode)

	var results = make([]*pb.Node, 0, len(path))

	//Convert model to proto object
	for _, node := range path {
		results = append(results, &pb.Node{
			Id:   node.ID.Hex(),
			Name: node.Name,
			Type: node.Type,
			Coordinate: &pb.Coordinate{
				X: node.Coordinate.X,
				Y: node.Coordinate.Y,
				Z: node.Coordinate.Z,
			},
			FloorID: node.FloorID,
			Details: &pb.Details{
				Description: node.Details.Description,
				Logoid:      node.Details.LogoID.Hex(),
				Imageid:     node.Details.ImageID.Hex(),
				Location:    node.Details.Location,
				Category:    node.Details.Category,
				Tags:        node.Details.Tags,
				OpeningHours: &pb.OpeningHours{
					Sunday:    node.Details.OpeningHours.Sunday,
					Monday:    node.Details.OpeningHours.Monday,
					Tuesday:   node.Details.OpeningHours.Tuesday,
					Wednesday: node.Details.OpeningHours.Wednesday,
					Thursday:  node.Details.OpeningHours.Thursday,
					Friday:    node.Details.OpeningHours.Friday,
					Saturday:  node.Details.OpeningHours.Saturday,
				},
			},
		})
	}

	return results, nil
}
